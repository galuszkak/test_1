#!flask/bin/python
import os
import unittest

from flask_login import login_user

from config import basedir
from app import app, db
from app.models import Account, User


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
                                                os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()
        self.user = User(name='aaa', surname='aaa',  email='aaa@aaa.pl',
                                                       password='12345')

        self.account = Account(owner_id=1, number='12345', balance=1000)

        db.session.add(self.user)
        db.session.add(self.account)
        db.session.commit()

        login_user(self.user)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_transfer_fail(self):
        response = self.app.post('/transfer', data=dict(amount=2000,
                                                        to_user=self.user,
                                                        ))
        self.assertEqual(response.status_code, 204)

    def test_transfer_ok(self):
        response = self.app.post('/transfer', data=dict(amount=200,
                                                       to_user=self.user,
                                                       ))
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()