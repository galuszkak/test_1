from flask import render_template
from flask import request
from flask import url_for
from flask_login import current_user, login_required, login_user
from werkzeug.utils import redirect

from app import app
from app.models import User


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        user = User.query.filter_by(email=email).first()
        print(user)
        if user is None or user.password != password:
            error = 'Invalid Credentials. Please try again.'
        else:
            login_user(user)
            return redirect(url_for('transfer'))
    return render_template('login.html', error=error)


@app.route('/transfer', methods=['GET', 'POST'])
@login_required
def transfer():
    error = None
    if request.method == 'POST':
        amount = request.form['ammount']
        user_name = request.form['to_user']
        if amount == 0 or user_name == '':
            error = 'Please check values for amount or user'
        else:
            user_to = User.query.filter_by(name=user_name).first()
            if amount < current_user.accounts.amount:
                current_user.accounts.change_amount(amount * -1)
                user_to.accounts.change_ammount(amount)
                return redirect(url_for('summary'))
            else:
                return render_template('transfer.html'), 204
    # print(current_user.accounts)
    return render_template('transfer.html', error=error)


