from flask_login import UserMixin

from app import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    surname = db.Column(db.String(64), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(10), unique=True)
    accounts = db.relationship('Account', backref='user', lazy='dynamic')

    def __repr__(self):
        return 'User : {}, {}'.format(self.name, self.surname)


class Account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    number = db.Column(db.String(250), unique=True, nullable=False)
    balance = db.Column(db.Numeric(12, 2), nullable=False)

    def change_amount(self, value):
        self.balance += value
        db.session.add(self)
        db.session.commit()
